import interfaces.Circle;
import interfaces.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        // khai báo đối tượng Circle
        Circle circle = new Circle(2.0);

        System.out.println("Circle");
        System.out.println(circle.toString());

        // tính area của circle
        System.out.println("Area of circle 1: " + circle.getArea());
        // tính Perimeter của circle
        System.out.println("Perimeter of circle 1: " + circle.getPerimeter());

        // khai báo đối tượng rectangle
        Rectangle rectangle = new Rectangle(2.5, 1.5);

        System.out.println("Rectangle");
        System.out.println(rectangle.toString());

        // tính area của rectangle
        System.out.println("Area of Rectangle: " + rectangle.getArea());
        // tính Perimeter của rectangle
        System.out.println("Perimeter of Rectangle: " + rectangle.getPerimeter());
    }
}
