package interfaces;

public class Rectangle implements GeometricObject {
    private double width;
    private double length;

    //khởi tạo phương thức
    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    @Override
    public double getArea() {
        // TODO Auto-generated method stub
        return this.width*this.length;
    }

    @Override
    public double getPerimeter() {
        // TODO Auto-generated method stub
        return (width + length)*2;
    }

    @Override
    public String toString() {
        return "Rectangle [width=" + width + ", length=" + length + "]";
    }

    
    
}
