package interfaces;

public class Circle implements GeometricObject {
    private double radius;
    
    //khởi tạo phương thức

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }

    @Override
    public double getArea() {
        return  Math.PI * Math.pow(this.radius,2);
    }

    @Override
    public double getPerimeter() {
        // TODO Auto-generated method stub
        return 2 * Math.PI * radius;
    }

    


    
    
}
